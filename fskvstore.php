<?php

class File {

  private $file_name;
  private $file_pointer;
  private $mode;

  public function __construct($file_name, $mode = 'r') {
    $this->mode = $mode;
    $this->file_name = $file_name;
    $this->file_pointer = fopen($file_name, $this->mode);
  }
  
  public function getContent() {
    $content = fread($this->file_pointer, filesize($this->file_name));
    fclose($this->file_pointer);
    return $content;
  }

  public function setContent($content) {
    return fwrite($this->file_pointer, $content);
  }

}


class FSKVStore {

  private $settings = array(
    'relative' => true,
    'root' => '~',
    'directory_separator' => '/'
  );

  public function __construct($options) {
    $this->settings = array_merge($this->settings, $options);
  }

// publics

  public function read($filename) {
    $full_path = $this->get_full_path($filename);
    $file = new File($full_path);
    return $file;
  }

  public function write($filename, $content) {
    $full_path = $this->get_full_path($filename);
    $file = new File($full_path, 'w+');
    return $file->setContent($content);
  }

// privates

  private function get_full_path($filename) {
    return $this->settings['root'].$this->settings['directory_separator'].$filename;
  }

}


